import math

#Inserction Sort

def inserction(array):

    lenght = len(array)

    for i in range(1, lenght):
        index = i-1
        key = array[i]

        while array[index] > key and index >= 0:
            array[index+1] = array[index]
            index -= 1

        array[index+1] = key

    return array

#Selection Sort

def selection(array):

    lenght = len(array)

    for i in range(0, lenght):
        key = array[i]
        index_key = i

        for index in range(i+1,lenght):

            if array[index] < key:
                key = array[index]
                index_key = index

        array[index_key] = array[i]
        array[i] = key

    return array

#Quick Sort

def quick(array):
    length = len(array)
    if length <= 1:
        return array
    else:
        pivot = array.pop()

    items_bigger = []
    items_smaller = []

    for item in array:
        if item > pivot:
            items_bigger.append(item)
        
        else:
            items_smaller.append(item)

    return quick(items_smaller) + [pivot] + quick(items_bigger)

#Merge Sort

def merge(array):

    length = len(array)

    if length <= 1:
        return array

    else:
        mid = math.floor(length/2)

        left_part = array[:int(mid)]
        right_part = array[int(mid):]

        merge(left_part)
        merge(right_part)

        left_index = 0
        right_index = 0
        key_index = 0

        while left_index < len(left_part) and right_index < len(right_part):

            if right_part[right_index] > left_part[left_index]:
                array[key_index] = left_part[left_index]
                left_index += 1
                key_index += 1
            else:
                array[key_index] = right_part[right_index]
                right_index += 1
                key_index += 1

        while left_index < len(left_part):
            array[key_index] = left_part[left_index]
            left_index += 1
            key_index += 1
        
        while right_index < len(right_part):
            array[key_index] = right_part[right_index]
            key_index += 1
            right_index += 1


        return array
    
#Maybe Merge Sort

def maybe_merge(array):

    length = len(array)

    if length <= 1:
        return array

    else:
        mid = math.floor(length/2)

        left_part = array[:int(mid)]
        right_part = array[int(mid):]

        #left_lengh = len(left_part)
        #right_lengh = len(right_part)
        #Tentei substituir len(left_part) por left_lengh (como para o right), mas assim da erro de 
        #Out of Index, depois me explica o porque.

    
        maybe_merge(left_part)
        maybe_merge(right_part)

        index = 0

        while index < length:
            
            if len(left_part) == 0:
                array[index] = right_part.pop(0)

            elif len(right_part) == 0:
                array[index] = left_part.pop(0)

            else:
                if left_part[0] < right_part[0]:
                    array[index] = left_part.pop(0)
                
                else:
                    array[index] = right_part.pop(0)
            
            index += 1
              
        return array

